﻿using EmpresasAPIDotNET.Data.Model;
using EmpressasAPIDotNET.Data.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EmpressasAPIDotNET.Services.Contracts
{
    public interface IEnterpriseService
    {
        Enterprise Get(int id);
        IEnumerable<Enterprise> GetAllByTypeAndName(int? enterpriseTypeID, string name);
    }
}
