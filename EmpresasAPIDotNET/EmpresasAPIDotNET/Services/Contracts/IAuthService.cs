﻿using EmpressasAPIDotNET.Data.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EmpressasAPIDotNET.Services.Contracts
{
    public interface IAuthService
    {
        User Login(string email, string password);
        UserSession CreateSession(User user);
        bool IsSessionValid(string uid, string client, string accessToken, out string message);
    }
}
