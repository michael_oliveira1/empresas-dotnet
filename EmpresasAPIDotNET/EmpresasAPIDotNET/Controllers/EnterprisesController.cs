﻿using Microsoft.AspNetCore.Mvc;
using EmpressasAPIDotNET.Filters;
using EmpressasAPIDotNET.Services.Contracts;
using EmpressasAPIDotNET.ViewModels;
using System;
using System.Net;

namespace EmpressasAPIDotNET.Controllers
{
    [Route("api/v1/enterprises")]
    [ApiController]
    [ServiceFilter(typeof(AuthenticationFilter))]
    public class EnterprisesController : ControllerBase
    {
        readonly IEnterpriseService enterpriseService;

        public EnterprisesController(IEnterpriseService enterpriseService)
        {
            this.enterpriseService = enterpriseService;
        }

        [HttpGet]
        public ActionResult<EnterprisesResponse> Get(
            [FromQuery(Name ="enterprise_types")]int? enterpriseTypeID,
            [FromQuery(Name = "name")] string name)
        {
            return new EnterprisesResponse(enterpriseService.GetAllByTypeAndName(enterpriseTypeID, name));
        }

        [HttpGet("{id}")]
        public ActionResult<EnterpriseResponse> Get(int id)
        {
            var enterprise = enterpriseService.Get(id);

            if (enterprise != null)
            {
                return new EnterpriseResponse(enterprise);
            }
            else
            {
                return NotFound(new { Status = "404", Error = "Not Found" });
            }
        }
    }
}
