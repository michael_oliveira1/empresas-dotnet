﻿using Microsoft.EntityFrameworkCore;
using EmpressasAPIDotNET.Data.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using EmpresasAPIDotNET.Data.Model;

namespace EmpressasAPIDotNET.Data
{
    public class EnterpriseContext : DbContext
    {
        public EnterpriseContext(DbContextOptions options)
            : base(options)
        {

        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Enterprise>()
                .Property(c => c.OwnEnterprise)
                .HasConversion<int>();

            modelBuilder.Entity<User>()
                .HasIndex(nameof(User.Email))
                .IsUnique(true)
                .HasName("User_Email_Index");

            modelBuilder.Entity<UserSession>()
                .HasIndex(nameof(UserSession.AccessToken))
                .HasName("UserSession_AccessToken_Index");
        }

        public DbSet<Enterprise> Enterprises { get; set; }
        public DbSet<EnterpriseType> EnterpriseTypes { get; set; }

        public DbSet<User> Users { get; set; }
        public DbSet<UserSession> UserSessions { get; set; }
    }
}
