﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EmpressasAPIDotNET.Data.Model
{
    public class EnterpriseType
    {
        [JsonProperty("id")]
        public int ID { get; set; }
        
        [JsonProperty("enterprise_type_name")]
        public string EnterpriseTypeName { get; set; }
    }
}
