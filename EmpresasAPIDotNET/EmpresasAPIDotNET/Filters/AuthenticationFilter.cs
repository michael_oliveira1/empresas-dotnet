﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using Newtonsoft.Json;
using EmpressasAPIDotNET.Services.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace EmpressasAPIDotNET.Filters
{
    public class AuthenticationFilter : ActionFilterAttribute
    {
        readonly IAuthService authService;

        public AuthenticationFilter(IAuthService authService)
        {
            this.authService = authService;
        }

        public override void OnActionExecuting(ActionExecutingContext context)
        {
            string uid = context.HttpContext.Request.Headers["uid"];
            string client = context.HttpContext.Request.Headers["client"];
            string accessToken = context.HttpContext.Request.Headers["access-token"];

            if (!this.authService.IsSessionValid(uid, client, accessToken, out string message))
            {
                var result = new ContentResult();

                result.Content = JsonConvert.SerializeObject(new
                {
                    Errors = new string[] { message }
                });

                result.ContentType = "application/json";
                result.StatusCode = 401;

                context.Result = result;
            }

            base.OnActionExecuting(context);
        }
    }
}
